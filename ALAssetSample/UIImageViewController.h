//
//  UIImageViewController.h
//  ALAssetSample
//
//  Created by Snehal Mehta on 7/12/13.
//  Copyright (c) 2013 Testing. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageViewController : UIViewController

@property (nonatomic, strong) NSString *imageUrl;

@end
