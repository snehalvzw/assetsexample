//
//  ViewController.m
//  ALAssetSample
//
//  Created by Snehal Mehta on 7/12/13.
//  Copyright (c) 2013 Testing. All rights reserved.
//

#import "ViewController.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "UIImageViewController.h"

@interface ViewController ()

@property(nonatomic, strong) ALAssetsLibrary *assetsLibrary;
@property(nonatomic, assign) ALAssetsLibraryAccessFailureBlock	assetsFailureBlock;

@end

@implementation ViewController
@synthesize photoUrls;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if(self)
    {
        //custom
        
        self.photoUrls = [NSMutableArray array];
		self.assetsLibrary = [[ALAssetsLibrary alloc] init];
        
        self.assetsFailureBlock = ^(NSError* error) {
            NSLog(@"There is some error %@ ",[error description]);
		};
        
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    ALAuthorizationStatus status = [ALAssetsLibrary authorizationStatus];

    switch (status) {
        case ALAuthorizationStatusAuthorized:
        {
            [self fetchPhotos];
        }
            break;
        case ALAuthorizationStatusNotDetermined:
        {
            [self fetchPhotos];
        }
            break;
        default:
            break;
    }
    
}

- (void)fetchPhotos {
	
	ALAssetsLibraryGroupsEnumerationResultsBlock groupBlock = ^(ALAssetsGroup *group, BOOL *stop) {
				
		if (group != nil) {
			*stop = YES;
		}
		
		[group enumerateAssetsUsingBlock:^(ALAsset *result, NSUInteger index, BOOL *stop) {

			if (result != nil) {
                
                NSLog(@"%@",[result valueForProperty:ALAssetPropertyURLs]);
                
				[self.photoUrls addObject:[NSString stringWithFormat:@"%@",[result valueForProperty:ALAssetPropertyAssetURL]]];
                
			} else {
				dispatch_async(dispatch_get_main_queue(), ^{
					[urlTableView reloadData];
				});
			}
		}];
	};
	
	[self.assetsLibrary enumerateGroupsWithTypes:ALAssetsGroupSavedPhotos
								usingBlock:groupBlock
							  failureBlock:nil];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

	return [self.photoUrls count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
		static NSString *reusableCellId = @"Cell";
    
		UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reusableCellId];
		
		if (cell == nil) {
			cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reusableCellId];
			cell.selectionStyle = UITableViewCellSelectionStyleBlue;
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
		}
		
    if([self.photoUrls count] > 0)
    {
        cell.textLabel.text = [self.photoUrls objectAtIndex:indexPath.row];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIImageViewController *imageVC = [[UIImageViewController alloc] initWithNibName:@"UIImageViewController" bundle:[NSBundle mainBundle]];
    
    imageVC.imageUrl = [self.photoUrls objectAtIndex:indexPath.row];
    
    [self.navigationController pushViewController:imageVC animated:YES];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
