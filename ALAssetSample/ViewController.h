//
//  ViewController.h
//  ALAssetSample
//
//  Created by Snehal Mehta on 7/12/13.
//  Copyright (c) 2013 Testing. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UITableViewDataSource,UITableViewDelegate>

{
    IBOutlet UITableView *urlTableView;
}

@property(nonatomic,strong) NSMutableArray *photoUrls;


@end
