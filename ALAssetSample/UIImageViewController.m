//
//  UIImageViewController.m
//  ALAssetSample
//
//  Created by Snehal Mehta on 7/12/13.
//  Copyright (c) 2013 Testing. All rights reserved.
//

#import "UIImageViewController.h"
#import <AssetsLibrary/AssetsLibrary.h>

@interface UIImageViewController ()

@end

@implementation UIImageViewController

@synthesize imageUrl;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    NSURL *assetUrl = [NSURL URLWithString:self.imageUrl];
    
    ALAssetsLibrary *assetLibrary = [[ALAssetsLibrary alloc] init];
    
    [assetLibrary assetForURL:assetUrl resultBlock:^(ALAsset *asset){
        
        CGImageRef imageRef = [[asset defaultRepresentation] fullScreenImage];
        
        UIImage *assetImage = [UIImage imageWithCGImage:imageRef];
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:self.view.bounds];
        
        [imageView setImage:assetImage];
        
        [self.view addSubview:imageView];
        
    } failureBlock:^(NSError *error){
        NSLog(@"Error %@ ",[error description]);
    }];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
