//
//  AppDelegate.h
//  ALAssetSample
//
//  Created by Snehal Mehta on 7/12/13.
//  Copyright (c) 2013 Testing. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) ViewController *viewController;

@property (nonatomic, strong) UINavigationController *navController;

@end
